<?php
/**
 * Elementor Accordion WordPress Plugin
 *
 * @package ElementorAccordion
 *
 * Plugin Name: Elementor Accordion
 * Version:     1.0.0
 * Text Domain: elementor-accordion
 */

define( 'ELEMENTOR_ACCORDION', __FILE__ );

/**
 * Include the Elementor_Accordion class.
 */
require plugin_dir_path( ELEMENTOR_ACCORDION ) . 'class-elementor-accordion.php';