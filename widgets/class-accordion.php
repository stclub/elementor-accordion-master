<?php
/**
 * Accordion class.
 *
 * @category   Class
 * @package    ElementorAccordion
 * @subpackage WordPress
 * @since      1.0.0
 * php version 7.3.9
 */

namespace ElementorAccordion\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

// Security Note: Blocks direct access to the plugin PHP files.
defined( 'ABSPATH' ) || die();

/**
 * Accordion widget class.
 *
 * @since 1.0.0
 */

class Accordion extends Widget_Base {
	/**
	 * Class constructor.
	 *
	 * @param array $data Widget data.
	 * @param array $args Widget arguments.
	 */
	 
	public function __construct( $data = array(), $args = null ) {
		parent::__construct( $data, $args );

		wp_register_style( 'accordion', plugins_url( '/assets/css/accordion.css', ELEMENTOR_ACCORDION ), array(), '1.0.0' );
		wp_enqueue_script( 'accordion', plugins_url( '/assets/js/accordion.js', ELEMENTOR_ACCORDION ), array(), '1.0.0' );
	}

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'accordion';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Accordion', 'elementor-accordion' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fa fa-pencil';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return array( 'general' );
	}
	
	/**
	 * Enqueue styles.
	 */
	public function get_style_depends() {
		return array( 'accordion' );
	}
    
	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
//	class Elementor_Test_Widget extends \Elementor\Widget_Base {

    protected function _register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => __( 'Content', 'elementor-accordion' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'list_title', [
				'label' => __( 'Title', 'elementor-accordion' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'List Title' , 'elementor-accordion' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'list_description', [
				'label' => __( 'Description', 'elementor-accordion' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'default' => __( 'list Description' , 'elementor-accordion' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'list_content', [
				'label' => __( 'Content', 'elementor-accordion' ),
				'type' => \Elementor\Controls_Manager::WYSIWYG,
				'default' => __( 'List Content' , 'elementor-accordion' ),
				'show_label' => false,
			]
		);

		$repeater->add_control(
			'list_color',
			[
				'label' => __( 'Color', 'elementor-accordion' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} {{CURRENT_ITEM}}' => 'color: {{VALUE}}'
				],
			]
		);

		$this->add_control(
			'list',
			[
				'label' => __( 'Repeater List', 'elementor-accordion' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [],
				'title_field' => '{{{ list_title }}}',
			]
		);

		$this->end_controls_section();

	}

	protected function render() {
		$settings = $this->get_settings_for_display();
		
		if ( $settings['list'] ) {
			
			?>
			<div class="container_stc">
				<style type="text/css">
					.stretch_img_1 {
						
						width: auto;
						font-family: Gotham Pro;
						font-style: normal;
						font-weight: 500;
						font-size: 30px;
						line-height: 1.5;
						width:250px;
						/* or 44px */
						text-align: center;
						color: #FFFFFF;
					}
					.container_stc .card_stc:hover > .artigo_nome_stc {
						  text-align: center;
						  flex-grow: 10;
						  top: calc(100% - 2em);
						  color: white;
						  position: absolute;
						font-family: Gotham Pro;
						font-style: normal;
						font-weight: bold;
						line-height: 110%;
						/* or 33px */
						color: #FFFFFF;
						transform: rotate(-90deg);
						}
				</style>
		<?php
			if (isset($_SERVER['HTTP_USER_AGENT'])) {
    			$agent = $_SERVER['HTTP_USER_AGENT'];
			}
			
			if (strlen(strstr($agent, 'Firefox')) > 0) {
					
    		?>
				<style>
					
					
				</style>
			<?php
			}
			if (strlen(strstr($agent, 'Chrome')) > 0) {
					
    		?>
				<style>
					
					
				</style>
			<?php
			}
			if (strlen(strstr($agent, 'OPR')) > 0) {
					
    		?>
				<style>
					
					
				</style>
			<?php
			}
			$img_count = 1;
			foreach (  $settings['list'] as $item ) {
		    
				?>
			<div class="card_stc">

		<?php
				 echo "<script type='text/javascript'>
				 if(jQuery(window).width() < 768) {
	jQuery('.artigo_nome_stc').css('position', 'absolute');
	
}
			jQuery('.container_stc .card_stc').bind('mouseover', function () { jQuery('.artigo_nome_stc').css('transform', 'rotate(-90deg)'); 
jQuery('.artigo_nome_stc').css('width', '250px');
jQuery('.artigo_nome_stc').css('padding', '5px');
jQuery('.artigo_nome_stc').css('position', 'absolute');
jQuery('.container_stc .card_stc .img_class p img').css('margin-bottom', '-100px');
if(jQuery(window).width() < 768) {
	jQuery('.artigo_nome_stc').css('width', '350px');
	jQuery('.artigo_nome_stc').css('padding', '0px 20px 30px 20px');
	jQuery('.container_stc .card_stc .img_class p img').css('margin-bottom', '0px');
	jQuery('.artigo_nome_stc').css('position', 'absolute');
}
if(jQuery(window).width() > 768 && jQuery(window).width() < 1024) {
	jQuery('.artigo_nome_stc').css('width', '470px');
	jQuery('.artigo_nome_stc').css('max-width', '475px');
	jQuery('.artigo_nome_stc').css('margin-top', '177px');
	jQuery('.container_stc .card_stc .img_class p img').css('margin-bottom', '-220px');	
}
});
jQuery('.container_stc .card_stc').bind('mouseout', function () { jQuery('.artigo_nome_stc').css('transform', 'rotate(0deg)');
	jQuery('.artigo_nome_stc').css('width', 'auto');
	jQuery('.artigo_nome_stc').css('padding', '30px');
	jQuery('.artigo_nome_stc').css('max-width', '250px');
	jQuery('.artigo_nome_stc').css('position', 'inherit');
	jQuery('.artigo_nome_stc').css('margin-top', '0px');
	jQuery('.container_stc .card_stc .img_class p img').css('margin-bottom', '-220px');
	if(jQuery(window).width() > 768 && jQuery(window).width() < 1024) {
	jQuery('.artigo_nome_stc').css('width', 'auto');
	jQuery('.artigo_nome_stc').css('padding', '5px');
		
}
	if(jQuery(window).width() < 768) {
	jQuery('.container_stc .card_stc .img_class p img').css('margin-bottom', '0px');
	jQuery('.artigo_nome_stc').css('position', 'absolute');
}
	});
	 </script>";
				echo '<div class="img_class">' . $item['list_content'] . '</div>';
				?>
				<div class="vector_2">
<div class="vector">
	
	<span class="head">
		Family, parenting & relationship
	 <p class="v_te" style="line-height: inherit">
	    <strong> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mi in nulla posuere sollicitudin aliquam ultrices sagittis orci.</strong></p>
	    
	 </span>
		<svg class="vector_size" viewBox="0 0 587 309" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M0 0V288.282L81.5066 288.17C82.8102 288.17 84.1021 288.17 85.394 288.282C102.647 289.149 117.962 296.081 125.243 306.411L126.5 308.196L127.757 306.411C135.038 296.073 150.341 289.184 167.606 288.282C168.886 288.213 170.19 288.17 171.493 288.17L587 288.282V0H0Z" fill="#0A5C4D" fill-opacity="0.9"/>
</svg>
</div>
<span class="rec"></span>
</div>

		<?php
		echo '<span class="stretch_img_1 artigo_nome_stc elementor-repeater-item-' . $item['_id'] . '"><p style="font-family: sans-serif;"><strong style="white-space: initial;font-size: 20px;">' . $item['list_title'] . '</strong></p>
</span>';
		?>
		</div>
				<?php
			}
			?>
			</div>
			<?php
		}
	}

	protected function _content_template() {
		?>
		
		<# if ( settings.list.length ) { #>
		
			<# _.each( settings.list, function( item ) { #>
				
				{{{ item.list_content }}}
			<# }); #>
		<# } #>
		<?php
	}
	

}